package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.ICommandRepository;
import ru.t1.semikolenov.tm.api.service.ICommandService;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        if (argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}