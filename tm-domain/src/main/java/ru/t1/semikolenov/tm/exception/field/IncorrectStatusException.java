package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String message) {
        super("Error! Status is incorrect! Status `" + message + "` not supported...");
    }

}
