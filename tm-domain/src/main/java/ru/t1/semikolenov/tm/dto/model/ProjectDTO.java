package ru.t1.semikolenov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

}
