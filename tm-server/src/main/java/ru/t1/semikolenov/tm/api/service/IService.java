package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}