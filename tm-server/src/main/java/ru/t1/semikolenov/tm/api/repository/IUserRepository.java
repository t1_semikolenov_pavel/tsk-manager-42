package ru.t1.semikolenov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull UserDTO user);

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void addAll(@NotNull Collection<UserDTO> users);

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, first_name = #{firstName},"
            + "last_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{locked} "
            + "WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Override
    @Delete("DELETE FROM tm_user")
    void clear();

    @Override
    @NotNull
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<UserDTO> findAll();

    @Override
    @Select("SELECT count(1) = 1 FROM tm_user WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM tm_user "
            + "WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneById(@Param("id") @NotNull String id);

    @Override
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull UserDTO user);

    @Override
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Override
    @Select("SELECT count(1) FROM tm_user")
    long getCount();

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM tm_user "
            + "WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM tm_user "
            + "WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByEmail(@Param("email") @NotNull String email);

}