package ru.t1.semikolenov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @Insert("INSERT INTO tm_task (id, user_id, project_id, name, description, status, created, date_begin, date_end)"
            + "VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull TaskDTO task);

    @Insert("INSERT INTO tm_task (id, user_id, project_id, name, description, status, created, date_begin, date_end)"
            + "VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<TaskDTO> tasks);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, user_id, project_id, name, description, status, created, date_begin, date_end FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<TaskDTO> findAll();

    @NotNull
    @Select("SELECT id, user_id, project_id, name, description, status, created, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<TaskDTO> findAllByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<TaskDTO> findAllComparator(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM tm_task WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, user_id, project_id, name, description, status, created, date_begin, date_end FROM tm_task "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    TaskDTO findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, user_id, project_id, name, description, status, created, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    TaskDTO findOneByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void remove(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<TaskDTO> collection);

    @Update("UPDATE tm_task SET project_id = #{projectId}, name = #{name}, description = #{description}, "
            + "status = #{status}, created = #{created}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    void update(@NotNull TaskDTO task);

    @Select("SELECT count(1) FROM tm_task")
    long getCount();

    @Select("SELECT count(1) FROM tm_task WHERE user_id = #{userId}")
    long getCountByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, user_id, project_id, name, description, status, created, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<TaskDTO> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Delete("DELETE FROM tm_task WHERE project_id = #{projectId}")
    void removeTasksByProjectId(@Param("projectId") @NotNull String projectId);

}