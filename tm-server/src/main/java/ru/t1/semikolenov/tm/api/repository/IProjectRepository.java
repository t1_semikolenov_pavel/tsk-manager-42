package ru.t1.semikolenov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Insert("INSERT INTO tm_project (id, user_id, name, description, status, created, date_begin, date_end)"
            + "VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull ProjectDTO project);

    @Insert("INSERT INTO tm_project (id, user_id, name, description, status, created, date_begin, date_end)"
            + "VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<ProjectDTO> projects);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, date_begin, date_end FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT id, user_id, name, description, status, created, date_begin, date_end FROM tm_project "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<ProjectDTO> findAllByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<ProjectDTO> findAllComparatorByUserId(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM tm_project WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, date_begin, date_end FROM tm_project "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    ProjectDTO findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, created, date_begin, date_end FROM tm_project "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    ProjectDTO findOneByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<ProjectDTO> collection);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, "
            + "status = #{status}, created = #{created}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    void update(@NotNull ProjectDTO project);

    @Select("SELECT count(1) FROM tm_project")
    long getCount();

    @Select("SELECT count(1) FROM tm_project WHERE user_id = #{userId}")
    long getCountByUserId(@Param("userId") @Nullable String userId);

}