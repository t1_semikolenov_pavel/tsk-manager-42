package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.api.repository.IUserOwnedRepository;
import ru.t1.semikolenov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {

}